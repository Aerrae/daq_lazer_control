function [x,y] = create_xy_map(filename, target_width, target_height)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
bmp_data = importdata(filename);
bmp_data = rot90(bmp_data.cdata, 3);
px_width = size(bmp_data, 1);
px_height = size(bmp_data, 2);
imageratio = px_width / px_height;
target_ratio = target_width/target_height;
if (imageratio < target_ratio)
    width_increment = target_width/px_width;
    target_height = target_width*imageratio;
    height_increment = target_height/px_height;

else
    height_increment = target_height/px_height;
    target_width = target_width/imageratio;
    width_increment = target_width/px_width;
    
end
left_edge = -px_width*width_increment/2;
top_edge = -px_height*height_increment/2;


x = [];
y = [];
for N = 1:px_width
    for M = 1:px_height
        if (bmp_data(N, M) == 0)
            x = [x, left_edge +  N*width_increment];
            y = [y, top_edge +  M*height_increment];
        end
    end
end

end

