
function [angle1_interp, angle2_interp] = calculate_angles(x, y, distance1, distance2)
    %UNTITLED Summary of this function goes here
    %   Detailed explanation goes here
    global FREQ_DAQ FREQ_LAZER
    angle1 = asind(x/(distance1 + distance2));
    angle2 = asind(y/(distance2));
    
    angle1_interp =  [];
    angle1_interp(1:round(FREQ_DAQ/FREQ_LAZER)) = angle1(1);
    for ang = 1:1:length(angle1)-1
        angle1_interp = [angle1_interp, interp_angles(angle1(ang), angle1(ang + 1))];
    end

    angle2_interp =  [];
    angle2_interp(1:round(FREQ_DAQ/FREQ_LAZER)) = angle2(1); 
    for ang = 1:1:length(angle1)-1
        angle2_interp = [angle2_interp, interp_angles(angle2(ang), angle2(ang + 1))];
    end

end


function  angles = interp_angles(start_angle, end_angle)
    global ANGLE_LIMIT_1K FREQ_DAQ FREQ_LAZER
    numinterp = (abs(end_angle - start_angle)/ANGLE_LIMIT_1K) + 1;
    if numinterp > (FREQ_DAQ/FREQ_LAZER)
        
        error(sprintf("Too many values %d to interpolate with start_angle %d end_angle %d ", numinterp, start_angle, end_angle))
    end
    angles = interp1([0, numinterp], [start_angle, end_angle], 1:numinterp);
    angles(length(angles)+1:round(FREQ_DAQ/FREQ_LAZER)) = end_angle;
    if max(diff(abs(angles))) >= ANGLE_LIMIT_1K + 0.01
        error("Too big interpolated angle, interp_angles algorithm not working correctly or global variables mismatching")
    end
end