function [x,y] = spiral(dotsize,radius)
%UNTITLED3 Summary of this function goes here
%   Detailed explanation goes here
x = [0];
y = [0];
c_rad = dotsize*2;
angle = 0;
while 1
    xs = cosd(angle)*c_rad;
    ys = sind(angle)*c_rad;
    ang_diff = asind(dotsize/c_rad);
    angle = mod(angle + ang_diff, 360);
    c_rad = c_rad + ang_diff/360*2*dotsize; 
    
    x = [x, xs];
    y = [y, ys];
    if c_rad > radius
        break
    end
    
end
end

