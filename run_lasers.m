target_width = 3;
target_height = 3;
d_mirror = 2;
d_target = 16;

duration_in_seconds = 1;

global ANGLE_LIMIT_1K FREQ_DAQ FREQ_LAZER MIRROR1_VOLTAGE_LIMITS MIRROR2_VOLTAGE_LIMITS;
global VOLTAGE_PER_DEGREE VOLTAGE_CHANGE_LIMIT;
ANGLE_LIMIT_1K = 0.2; %from galvomirror specification for 1khz, in degrees
VOLTAGE_PER_DEGREE = 0.5; % controlled by jumper inside lazer controller
VOLTAGE_CHANGE_LIMIT = ANGLE_LIMIT_1K*VOLTAGE_PER_DEGREE;
FREQ_DAQ = 1000;
FREQ_LAZER = 10; %100ms interval between pulses
MIRROR1_VOLTAGE_LIMITS = [-10, 10];
MIRROR2_VOLTAGE_LIMITS = [-10, 10];
TEST = 1;

%[x,y] = create_xy_map("testikuva3.bmp", target_width, target_height); scatter(x,y); xlim([-5, 5]); ylim([-5,5])
%[x,y] = box();
%[x,y] = rectspiral(0.01, 1);
[x,y] = spiral(0.01, 2);

pattern_length_in_seconds = length(x)/FREQ_LAZER;
repetitions = ceil(duration_in_seconds / pattern_length_in_seconds);
sprintf('Number of repetitions: %d', repetitions)

x_rep = repmat(x,1,ceil(repetitions));
y_rep = repmat(y,1,ceil(repetitions));

[a1, a2] = calculate_angles(x_rep, y_rep, d_mirror, d_target);

V1 = angle_to_voltage(a1);
sprintf('Minimum %f and maximum %f voltages: ', min(V1), max(V1))
if max(V1) >= MIRROR1_VOLTAGE_LIMITS(2) || min(V1) <= MIRROR1_VOLTAGE_LIMITS(1)
    sprintf("max V1: %f, min V1: %f, Limit V1 min %f max %f", max(V1), min(V1), MIRROR1_VOLTAGE_LIMITS(1), MIRROR1_VOLTAGE_LIMITS(2))
    error("Too big voltages for mirror1 (X-axis)")
end

V2 = angle_to_voltage(a2);
if max(V2) >= MIRROR2_VOLTAGE_LIMITS(2) || min(V2) <= MIRROR2_VOLTAGE_LIMITS(1)
    sprintf("max V2: %f, min V2: %f, Limit V2 min %f max %f", max(V2), min(V2), MIRROR2_VOLTAGE_LIMITS(1), MIRROR2_VOLTAGE_LIMITS(2))
    error("Too big voltages for mirror2 (Y-axis)")
end

if (~TEST)
    if (not(exist("dq")))
        dq = daq("ni");
        dq.Rate = FREQ_DAQ;
        addoutput(dq, "Dev1", "ao0", "Voltage");
        addoutput(dq, "Dev1", "ao1", "Voltage");
    end
    write(dq, [V1',V2']);
else
    sprintf('Running simulated test for DAQ, position is calculated from voltages')
    [x,y] = calculate_position(a1, a2, d_mirror, d_target);
    plot(x,y)
    title('Expected pattern, calculated from voltages')
end