function [x, y] = rectspiral(dotsize, side_length)
%UNTITLED Summary of this function goes here
x = [];
y = [];
gr_int = (dotsize)/side_length;
while 1
    if side_length < 0
        break
    end
    xmin = -side_length/2;
    xmax = side_length/2;
    ymin = -side_length/2;
    ymax = side_length/2;    

    %top edge
    xvec = xmin:gr_int:xmax;
    yvec = ymax*ones(1,length(xvec));
    x = [x, xvec];
    y = [y, yvec];
    %right edge
    yvec = ymax:-gr_int:ymin;
    xvec = xmax*ones(1,length(yvec));
    x = [x, xvec];
    y = [y, yvec];
    
    %bottom edge
    xvec = xmax:-gr_int:xmin;
    yvec = ymin*ones(1,length(xvec));
    x = [x, xvec];
    y = [y, yvec];
    
    %left edge
    yvec = ymin:gr_int:(ymax-gr_int);
    xvec = xmin*ones(1,length(yvec));
    x = [x, xvec];
    y = [y, yvec];
    
    %new square side length
    side_length = side_length - 2*dotsize;
    
end
end

